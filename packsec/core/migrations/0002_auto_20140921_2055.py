# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import uuidfield.fields


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='BaseModel',
            fields=[
                ('id', uuidfield.fields.UUIDField(primary_key=True, serialize=False, editable=False, max_length=32, blank=True, unique=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Language',
            fields=[
                ('basemodel_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='core.BaseModel')),
                ('name', models.CharField(max_length=255)),
            ],
            options={
            },
            bases=('core.basemodel',),
        ),
        migrations.CreateModel(
            name='LanguageVersion',
            fields=[
                ('basemodel_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='core.BaseModel')),
                ('version', models.CharField(max_length=255)),
                ('language', models.ForeignKey(to='core.Language')),
            ],
            options={
            },
            bases=('core.basemodel',),
        ),
        migrations.CreateModel(
            name='Library',
            fields=[
                ('basemodel_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='core.BaseModel')),
                ('name', models.CharField(max_length=255)),
                ('language', models.ForeignKey(to='core.Language')),
            ],
            options={
            },
            bases=('core.basemodel',),
        ),
        migrations.CreateModel(
            name='LibraryVersion',
            fields=[
                ('basemodel_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='core.BaseModel')),
                ('version', models.CharField(max_length=255)),
                ('release_date', models.DateField()),
            ],
            options={
            },
            bases=('core.basemodel',),
        ),
        migrations.AlterField(
            model_name='user',
            name='date_joined',
            field=models.DateField(auto_now_add=True),
        ),
    ]
