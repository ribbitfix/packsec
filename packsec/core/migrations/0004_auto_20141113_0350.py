# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0003_project'),
    ]

    operations = [
        migrations.AddField(
            model_name='libraryversion',
            name='library',
            field=models.ForeignKey(default='1', to='core.Library'),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='libraryversion',
            name='release_date',
            field=models.DateField(null=True, blank=True),
        ),
    ]
