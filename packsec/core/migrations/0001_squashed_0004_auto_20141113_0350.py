# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.utils.timezone
from django.conf import settings
import uuidfield.fields


class Migration(migrations.Migration):

    replaces = [(b'core', '0001_initial'), (b'core', '0002_auto_20140921_2055'), (b'core', '0003_project'), (b'core', '0004_auto_20141113_0350')]

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='User',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('password', models.CharField(max_length=128, verbose_name='password')),
                ('last_login', models.DateTimeField(default=django.utils.timezone.now, verbose_name='last login')),
                ('email', models.EmailField(unique=True, max_length=500)),
                ('username', models.CharField(unique=True, max_length=500)),
                ('is_active', models.BooleanField(default=True)),
                ('is_staff', models.BooleanField(default=False)),
                ('is_superuser', models.BooleanField(default=False)),
                ('date_joined', models.DateField(auto_now_add=True)),
            ],
            options={
                'abstract': False,
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='BaseModel',
            fields=[
                ('id', uuidfield.fields.UUIDField(primary_key=True, serialize=False, editable=False, max_length=32, blank=True, unique=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Language',
            fields=[
                ('basemodel_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='core.BaseModel')),
                ('name', models.CharField(max_length=255)),
            ],
            options={
            },
            bases=('core.basemodel',),
        ),
        migrations.CreateModel(
            name='LanguageVersion',
            fields=[
                ('basemodel_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='core.BaseModel')),
                ('version', models.CharField(max_length=255)),
                ('language', models.ForeignKey(to='core.Language')),
            ],
            options={
            },
            bases=('core.basemodel',),
        ),
        migrations.CreateModel(
            name='Library',
            fields=[
                ('basemodel_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='core.BaseModel')),
                ('name', models.CharField(max_length=255)),
                ('language', models.ForeignKey(to='core.Language')),
            ],
            options={
            },
            bases=('core.basemodel',),
        ),
        migrations.CreateModel(
            name='LibraryVersion',
            fields=[
                ('basemodel_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='core.BaseModel')),
                ('version', models.CharField(max_length=255)),
                ('release_date', models.DateField(null=True, blank=True)),
                ('library', models.ForeignKey(default='1', to='core.Library')),
            ],
            options={
            },
            bases=('core.basemodel',),
        ),
        migrations.CreateModel(
            name='Project',
            fields=[
                ('basemodel_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='core.BaseModel')),
                ('name', models.CharField(max_length=255)),
                ('owner', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
            options={
            },
            bases=('core.basemodel',),
        ),
    ]
