# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0001_squashed_0004_auto_20141113_0350'),
    ]

    operations = [
        migrations.AddField(
            model_name='project',
            name='libvs',
            field=models.ManyToManyField(to='core.LibraryVersion', null=True, blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='libraryversion',
            name='library',
            field=models.ForeignKey(to='core.Library'),
        ),
    ]
