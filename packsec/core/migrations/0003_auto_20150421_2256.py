# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0002_auto_20141123_0509'),
    ]

    operations = [
        migrations.RenameField(
            model_name='project',
            old_name='libvs',
            new_name='library_versions',
        ),
    ]
