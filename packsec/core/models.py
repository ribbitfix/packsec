from django.db import models
from django.contrib.auth.models import AbstractBaseUser, UserManager
from uuidfield import UUIDField
from core.libversiongetter import LibraryVersionGetter


class User(AbstractBaseUser):
    USERNAME_FIELD = 'username'
    IDENTIFICATION = 'email'
    email = models.EmailField(max_length=500, unique=True)
    username = models.CharField(max_length=500, unique=True)
    is_active = models.BooleanField(default=True)
    is_superuser = models.BooleanField(default=False)
    is_staff = models.BooleanField(default=False)
    date_joined = models.DateField(auto_now_add=True)
    objects = UserManager()


class BaseModel(models.Model):
    id = UUIDField(auto=True, primary_key=True)

    @property
    def strid(self):
        return str(self.id)

# universal
class Language(BaseModel):
    name = models.CharField(max_length=255)


class LanguageVersion(BaseModel):
    version = models.CharField(max_length=255)
    language = models.ForeignKey(Language)

class Library(BaseModel):
    name = models.CharField(max_length=255)
    language = models.ForeignKey(Language)
    version_getter = None

    def _get_all_versions(self):
        return self.version_getter.search(self.name)

    def _create_unknown_versions(self, versions=None):
        if versions is None:
            versions = self._get_all_versions()
        for version in versions:
            LibraryVersion.objects.get_or_create(library=self, version=version['version'])

    def __init__(self, *args, **kwargs):
        super(Library, self).__init__(*args, **kwargs)
        self.version_getter = LibraryVersionGetter()

class LibraryVersionManager(models.Manager):
    def newer_versions(self, library_version):
        return self.filter(library=library_version.library).exclude(
            id=library_version.id).filter(release_date__gt=library_version.release_date
                                          ).order_by('-release_date')


class LibraryVersion(BaseModel):
    library = models.ForeignKey(Library)
    version = models.CharField(max_length=255)
    release_date = models.DateField(null=True, blank=True)
    objects = LibraryVersionManager()

    def __unicode__(self):
        return "%s version %s, released on %s" % (self.library.name, self.version, self.release_date)

#personal

class Project(BaseModel):
    name = models.CharField(max_length=255)
    owner = models.ForeignKey(User)

    library_versions = models.ManyToManyField(LibraryVersion, blank=True, null=True)

    @property
    def available_updates(self):
        available_updates = []
        for library_version in self.library_versions.all():
            newer_versions = LibraryVersion.objects.newer_versions(library_version)
            if newer_versions.exists():
                available_updates.append(newer_versions[0])

        return available_updates

