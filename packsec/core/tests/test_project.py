from unittest import TestCase
import datetime
from core.models import Project, User, Language, Library, LibraryVersion


class ProjectTests(TestCase):
    def setUp(self):
        self.owner, created = User.objects.get_or_create()
        self.project = Project.objects.create(name="foobar", owner=self.owner)

    def create_two_library_versions(self, library_name="barbar"):
        language = Language.objects.create(name="Python")
        lib = Library.objects.create(name=(library_name), language=language)
        libversion1 = LibraryVersion.objects.create(version='1.0', library=lib,
                                                    release_date=datetime.datetime.now() - datetime.timedelta(days=3))
        libversion2 = LibraryVersion.objects.create(version='2.0', library=lib,
                                                    release_date=datetime.datetime.now() - datetime.timedelta(days=1))
        return (libversion1, libversion2)

    def test_report_available_updates_reports_newer_library_versions(self):
        libv1, libv2 = self.create_two_library_versions()
        self.assertEqual([], self.project.available_updates)
        self.project.library_versions.add(libv1)
        self.assertEqual([libv2], self.project.available_updates)

    def test_newer_versions_returns_newer_versions_correctly(self):
        vandal1, vandal2 = self.create_two_library_versions("vandal")
        self.assertFalse(LibraryVersion.objects.newer_versions(vandal2).exists())
        self.assertEqual(vandal2, LibraryVersion.objects.newer_versions(vandal1)[0])

    def test_report_many_available_updates_reports_correctly(self):
        vandal1, vandal2 = self.create_two_library_versions('vandal')
        visigoth1, visigoth2 = self.create_two_library_versions('visigoth')

        self.assertEqual([], self.project.available_updates)

        self.project.library_versions.add(vandal1, visigoth1)

        updates = self.project.available_updates

        self.assertIn(vandal2, updates)
        self.assertIn(visigoth2, updates)
        self.assertEqual(2, len(updates))


    def test_report_many_available_updates_reports_correctly_for_one_updated_one_most_recent(self):
        vandal1, vandal2 = self.create_two_library_versions('vandal')
        visigoth1, visigoth2 = self.create_two_library_versions('visigoth')

        self.assertEqual([], self.project.available_updates)

        self.project.library_versions.add(vandal1, visigoth2)

        updates = self.project.available_updates
        self.assertEqual([vandal2], updates)



