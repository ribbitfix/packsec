from cStringIO import StringIO
from unittest import skip
from django.test import TestCase
from core.models import LibraryVersion, Library, Language, User, Project
from core.parser import RequirementsFileParser


class RequirementsFileParserTests(TestCase):

    def setUp(self):
        Language.objects.get_or_create(name='Python')
        self.requirements_file_data = """
        Django==1.7
        foobar==12
        """
        self.requirements_file_obj = StringIO(self.requirements_file_data)

    def test_basic_parsing_creates_packages(self):
        rfp = RequirementsFileParser(self.requirements_file_obj)
        self.assertTrue(rfp.is_valid())

        self.assertEqual(0, LibraryVersion.objects.all().count())
        self.assertEqual(0, Library.objects.all().count())

        rfp.instantiate()

        self.assertEqual(2, LibraryVersion.objects.all().count())
        self.assertEqual(2, Library.objects.all().count())

        lv1 = LibraryVersion.objects.get(library__name='Django')
        self.assertEqual('1.7', lv1.version)
        lv2 = LibraryVersion.objects.get(library__name='foobar')
        self.assertEqual('12', lv2.version)

    def test_reparsing_requirements_files_doesnt_create_copied_library_versions(self):
        rfp = RequirementsFileParser(self.requirements_file_obj)
        self.assertTrue(rfp.is_valid())

        rfp.instantiate()

        self.assertEqual(2, LibraryVersion.objects.all().count())
        self.assertEqual(2, Library.objects.all().count())
        self.requirements_file_obj.seek(0)

        rfp2 = RequirementsFileParser(self.requirements_file_obj)
        self.assertTrue(rfp2.is_valid())

        rfp2.instantiate()

        self.assertEqual(2, LibraryVersion.objects.all().count())
        self.assertEqual(2, Library.objects.all().count())


    @skip("")
    def test_parsing_can_handle_git_requirements(self):
        self.fail()

    @skip("")
    def test_parsing_can_handle_http_requirements(self):
        self.fail()

    def test_parser_attach_attaches_file_and_requirements_to_project(self):
        rfp = RequirementsFileParser(self.requirements_file_obj)
        self.assertTrue(rfp.is_valid())
        rfp.instantiate()
        user = User.objects.create()
        project = Project.objects.create(name='test', owner=user)

        self.assertEqual(0, project.library_versions.all().count())
        rfp.attach_library_versions(project)
        self.assertEqual(2, project.library_versions.all().count())
        lv1 = project.library_versions.get(library__name='Django')
        self.assertEqual('1.7', lv1.version)
        lv2 = project.library_versions.get(library__name='foobar')
        self.assertEqual('12', lv2.version)

