from django.test import TestCase
from core.models import Library, LibraryVersion


class MockServerProxy(object):
    def search(self, args, combiner):
        return [
            {'name': 'pip', 'version': 1},
            {'name': 'pip', 'version': 2},
            {'name': 'pipin', 'version': 3},
        ]

class VersionGetterTests(TestCase):
    def setUp(self):
        from core.models import Language
        self.python, created = Language.objects.get_or_create(name='Python')

    def test_can_get_all_versions_for_a_package(self):
        lib = Library.objects.create(name='pip', language=self.python)
        lib.version_getter.pypi = MockServerProxy()

        versions = lib._get_all_versions()
        self.assertEqual(2, len(versions))

    def test_can_create_versions_from_get_all_versions(self):
        lib = Library.objects.create(name='pip', language=self.python)
        lib.version_getter.pypi = MockServerProxy()

        self.assertEqual(0, LibraryVersion.objects.all().count())

        versions = lib._get_all_versions()
        lib._create_unknown_versions(versions)

        self.assertEqual(2, LibraryVersion.objects.all().count())
