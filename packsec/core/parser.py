from pkg_resources import parse_requirements
from core.models import Library, LibraryVersion, Language

__author__ = 'jeff'


class RequirementsFileParser(object):
    def __init__(self, reqfile):
        self.language = Language.objects.get(name='Python')
        self.reqfile = reqfile
        self.libversions = []

    def is_valid(self):
        try:
            self.reqs = parse_requirements(self.reqfile.read())
        except Exception, e:
            self.errors = e
            return False
        return True

    def instantiate(self):
        for req in self.reqs:
            lib, created = Library.objects.get_or_create(name=req.project_name, language=self.language)
            libversion, created = LibraryVersion.objects.get_or_create(version=req.specs[0][1], library=lib)
            self.libversions.append(libversion)

    def attach_library_versions(self, project):
        for libv in self.libversions:
            project.library_versions.add(libv)
