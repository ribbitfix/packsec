from django.core.urlresolvers import reverse
from django.http import Http404
from django.shortcuts import render, redirect, get_object_or_404
from django.views.generic import View

from .models import Project
from core.parser import RequirementsFileParser

def profile(request):
    return render(request, 'profile.html', {'projects': request.user.project_set.all()})

class NewProject(View):
    def get(self, request):
        return render(request, 'project/new.html')
    def post(self, request):
        project = Project.objects.create(
            name=request.POST['name'],
            owner=request.user,
            )
        return redirect(reverse('project_overview', args=[project.strid]))

def project_overview(request, id):
    project = get_object_or_404(Project, id=id, owner=request.user)
    return render(request, 'project/overview.html', {'project': project})

def project_file_upload(request, id):
    project = get_object_or_404(Project, id=id, owner=request.user)
    if not request.method == 'POST':
        raise Http404()
    rfps = []
    for reqfile in request.FILES['reqfile']:
        rfp = RequirementsFileParser(reqfile)
        if not rfp.is_valid():
            raise Exception("Invalid file")
        rfps.append(rfp)
    for rfp in rfps:
        rfp.instantiate()
        rfp.attach_library_versions(project)

    import ipdb; ipdb.set_trace()