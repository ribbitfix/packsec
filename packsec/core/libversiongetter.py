import xmlrpclib


class LibraryVersionGetter(object):
    index_url = pypi = None

    def __init__(self):
        self.index_url = 'https://pypi.python.org/pypi'
        self.pypi = xmlrpclib.ServerProxy(self.index_url)

    def search(self, lib_name):
        versions = self.pypi.search({'name': lib_name, 'summary': lib_name}, 'or')
        return [version for version in versions if version['name'] == lib_name ]
