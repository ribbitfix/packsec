from django.conf.urls import patterns, include, url
from django.contrib import admin
from django.shortcuts import render
from core import views as core_views

def home(request):
    return render(request, "home.html")

urlpatterns = patterns('',
    url(r'^$', home, name='home'),
    url(r'^accounts/profile/$', 'core.views.profile', name='profile'),
    url(r'^project/new/$', core_views.NewProject.as_view(), name='new_project'),
    url(r'^project/(?P<id>[^/]+)/$', 'core.views.project_overview', name='project_overview'),
    url(r'^project/(?P<id>[^/]+)/upload/$', 'core.views.project_file_upload', name='project_file_upload'),
    url(r'^admin/', include(admin.site.urls)),
    url('', include('social.apps.django_app.urls', namespace='social'))

)
